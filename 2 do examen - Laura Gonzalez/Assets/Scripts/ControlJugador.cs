using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("MusicaJuego");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GestorDeAudio.instancia.ReproducirSonido("Salto");
        }
    }
}